let vboton = document.querySelector("button")
vboton.addEventListener("click", cambiar)
    //con querySelector asignamos la etiqueta button, y luego con addEventListener
    // hacemos que al hacer click en el boton, se cargue la funcion cambiar.

function cambiar() {
    let caja = document.querySelector('#filete');
    let ancho = getComputedStyle(caja).getPropertyValue("width");
    let alto = getComputedStyle(caja).getPropertyValue("height");

    caja.style.width = 300 + "px";
    caja.style.height = 300 + "px";

}