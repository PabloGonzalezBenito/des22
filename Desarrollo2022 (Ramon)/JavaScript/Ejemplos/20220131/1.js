//document lo tenemos porque estamos en la web; en caso contrario, no habría document y daría error.

document.write ("Pablo"); //document: accedemos al document. write: escribimos en él. Estamos escribiendo en el body
console.log("Pablo"); //escribimos en la consola

//escribir en el div que tiene como id "caja"
document.querySelector("#caja").innerHTML="Pablo";

//métodos son acciones que hace un elemento (ej:levantarse, sentarse...) se ponen entre parentesis
//propiedades son características del elemento. (ej:edad, altura...) se ponen asi: elemento.propiedad