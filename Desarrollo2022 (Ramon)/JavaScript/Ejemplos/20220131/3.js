//definicion de las variables

let radio = null;
let perimetro = null;
let aera = null;

//Introducir los datos
radio = prompt("Introduce el radio");

//Procesamiento de la informacion
perimetro = 2 * Math.PI * radio;
area = Math.PI * Math.pow(radio, 2);

document.write("El radio es: " + radio + "<br>");
document.write("El perimetro es: " + perimetro + "<br>");
document.write(`El area es: ${area} <br>`);