//Creo una constante con mi nombre

const NOMBRE = "Pablo";
//creo una variable

let elemento = null;
document.write(NOMBRE);
console.log(NOMBRE);
//creamos una variable para acceder a la caja; elemento es lo mismo que div (apunta al div)
elemento = document.querySelector("#caja");
elemento.innerHTML = NOMBRE; //aprovecho la variable para modificar el div
document.querySelector("#caja").innerHTML = NOMBRE;
//null es cuando quieres que la variable exista pero no valga nada (inicializar); está creada
//undefined significa que esta sin definir; no existe.