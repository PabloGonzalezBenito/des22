function suma() {
    let a = parseInt(document.querySelector("#numero1").value);
    let b = parseInt(document.querySelector("#numero2").value);

    let c = a + b;
    let salida = document.querySelector("#salida");
    salida.innerHTML = c;

    let resultado = document.querySelector("#resultado");
    resultado.value = c;

}