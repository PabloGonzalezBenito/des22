window.getComputedStyle(document.querySelector('#salida').getPropertyValue("width"));

function cambiar(color) {
    let muestra = document.querySelector('#salida');
    muestra.style.backgroundColor = color;
}


function ancho(valor) {
    let muestra = document.querySelector('#salida');
    //leo el ancho del div independientemente de como se haya colocado el estilo
    let ancho = window.getComputedStyle(muestra).getPropertyValue("width");
    muestra.style.width = (parseInt(ancho) + valor).toString() + "px";
}