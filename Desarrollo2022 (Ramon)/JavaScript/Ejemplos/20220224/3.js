function operar(operacion) {
    let a = parseInt(document.querySelector("#numero1").value);
    let b = parseInt(document.querySelector("#numero2").value);
    let resultado = 0;
    if (operacion == "sumar") {
        resultado = a + b;
    } else {
        resultado = a - b;
    }
    salida(resultado);
}

function salida(valor) {
    let vResultado = document.querySelector('#resultado');
    vResultado.value = valor;
}