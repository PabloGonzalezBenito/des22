//definicion de las variables

let radio = 0;
let perimetro = 0;
let area = 0;
let circulo = null; //variable para apuntar al div que se llama circulo

//Introducir los datos
radio = prompt("Introduce el radio");

//Procesamiento de la informacion
perimetro = 2 * Math.PI * radio;
area = Math.PI * Math.pow(radio, 2);

document.write("El radio es: " + radio + "<br>");
document.write("El perimetro es: " + perimetro + "<br>");
document.write(`El area es: ${area} <br>`);

//dibujar circulo con radio dado
circulo = document.querySelector("#circulo");
circulo.style.width = radio + "px";
circulo.style.height = radio + "px";
circulo.style.backgroundColor = "blue";
circulo.style.borderRadius = radio + "px";