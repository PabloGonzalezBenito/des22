//Calculamos el area y el perimetro de un rectangulo mediante datos aportados por el usuario.

let base = 0;
let altura = 0;
let perimetro = 0;
let area = 0;

base = prompt("Introduce la BASE:");
altura = prompt("Introduce la ALTURA:");

perimetro = 2 * base + 2 * altura;
area = base * altura;

document.write("<div>El valor del área es " + area + " y el valor del perímetro es " + perimetro + "</div>");