//creando una clase de tipo Loro
let Loro = function() {
    //creando propiedades publicas (this es obligatorio)
    this.color = "rojo";
    this.peso = 10;
    this.constructor = function() {
            console.log("creando loro");


            //creando metodos publicos
            this.hablar = function() {
                return "piopio";
            }
            this.volar = function() {
                return "volando voy";
            }
        }
        //llamando al constructor
    this.constructor();
}

/*Creo dos objetos de tipo Loro*/

let loro1 = new Loro();
let loro2 = new Loro();


loro1.color = "Azul";