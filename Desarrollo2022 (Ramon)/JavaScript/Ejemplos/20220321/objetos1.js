/*objeto con los siguientes miembros
objeto loro 
MIEMBROS
    propiedades
        color
        peso
    metodos
        hablar
        volar
*/

//NOTACION JSON
let loro = {
    color: "azul",
    peso: 10,
    hablar: function() {
        return "piopio";
    },
    volar: function() {
        return "volando voy";
    }

};

// si fuera un array
let Loro = [
    "azul", 10,
    function() { return "piopio" },
    function() {
        return "volando voy"
    }
]

//acceder a los elementos del objeto LORO

//propiedad
console.log(`el color de mi loro es ${loro.color}`)

//metodo
console.log(loro.volar());


//acceder a los elementos del objeto LORO
//elemento color
console.log(Loro[0]);
//elemento volar
console.log(Loro[3]());

//modificar el color de mi loro
loro.color = "rojo";