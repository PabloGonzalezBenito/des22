/*
clase llamada persona con los miembros
nombre
edad
localidad*/
//utilizamos la clase mediante argumentos de entrada
/*const Persona=function(nombre = "", edad = 0, localidad = ""){
 this.constructor=function(){
this.nombre=nombre;
this.edad=edad;
this.localidad=localidad;
 }   
 this.constructor(nombre,edad,localidad);
}*/


class Persona {
    //las propiedades tienen que estar en el constructor,
    // de lo contrario da error.
    //utilizamos la clase mediante argumentos de entrada
    constructor(nombre = "", edad = 0, localidad = "") {
        this.nombre = nombre;
        this.edad = edad;
        this.localidad = localidad;
    }
}

const personas = [];
personas.push(new Persona(), new Persona("Ana Gonzalez"), new Persona("Luis Gomez", 45));