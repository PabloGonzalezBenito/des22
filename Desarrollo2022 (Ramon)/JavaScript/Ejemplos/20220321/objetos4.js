//creando una clase de tipo Loro
class Loro {
    constructor() {
            //creando propiedades publicas (this es obligatorio)
            this.color = "rojo";
            this.peso = 10;
            console.log("Creando loro");
        }
        //creando metodos publicos
    hablar() {
        return "piopio";
    }
    volar() {
        return "volando voy";
    }
}

/*Creo dos objetos de tipo Loro*/

const loro1 = new Loro();
const loro2 = new Loro();


loro1.color = "Azul";