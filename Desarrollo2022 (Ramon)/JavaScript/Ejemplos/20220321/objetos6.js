/*
clase llamada persona con los miembros
nombre
edad
localidad*/

/*const Persona=function(){
 this.constructor=function(){
this.nombre="";
this.edad=0;
this.localidad="";
 }   
 this.constructor();
}*/

class Persona {
    //las propiedades tienen que estar en el constructor, de lo contrario da error.
    constructor() {
        this.nombre = "";
        this.edad = 0;
        this.localidad = "";
    }
}

const personas = [];
personas.push(new Persona(), new Persona());
personas[0].nombre = "Ana";
personas[1].nombre = "Luis";