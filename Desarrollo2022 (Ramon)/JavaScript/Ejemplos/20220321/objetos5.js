//utilizar las 2 nomenclaturas vistas anteriormente para crear una clase.
//antigua (ejemplo3): Perro1. nueva (ejemplo4):Perro2

//propiedades:
/*
color
pelo
peso
fechaNacimiento


metodo antiguo:metemos todas dentro del constructor
*/


/*metodos:

ladrar("guau guau");
dormir("durmiendo");
*/



//ANTIGUO
let Perro1 = function() {
    this.color = "negro";
    this.pelo = true;
    this.peso = 15;
    this.fechaNacimiento = "21/03/2020";
    this.constructor = function() {
        console.log("creando perro");
    }

    this.ladrar = function() {
        return ("guau guau");
    }

    this.dormir = function() {
        return ("durmiendo");
    }

    this.constructor();

}

//NUEVO
class Perro2 {
    constructor() {
        this.color = "blanco";
        this.pelo = true;
        this.peso = 30;
        this.fechaNacimiento = "01/01/2015";
    }
    ladrar() { return ("guau guau"); }
    dormir() { return ("durmiendo"); }
}


//Creamos objetos de tipo Perro1 y Perro2
let perro1 = new Perro1();
let perro2 = new Perro2();