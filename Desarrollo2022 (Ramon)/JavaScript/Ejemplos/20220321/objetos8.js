/*
clase llamada persona con los miembros
nombre
edad
localidad*/
//utilizamos la clase mediante argumentos de entrada
/*const Persona=function(nombre = "", edad = 0, localidad = ""){
 this.constructor=function(){
this.nombre=nombre;
this.edad=edad;
this.localidad=localidad;
 }   
 this.constructor(nombre,edad,localidad);
}*/


class Persona {
    //utilizamos la clase mediante argumento de entrada JSON(un objeto)
    constructor(datos = {}) {
        /*if (datos.hasOwnProperty("nombre")) {
            this.nombre = datos.nombre;
        } else {
            this.nombre = "";
        }*/
        this.nombre = datos.hasOwnProperty("nombre") ? datos.nombre : ""; //ternario
        this.edad = datos.hasOwnProperty("edad") ? datos.edad : 0;
        this.localidad = datos.hasOwnProperty("localidad") ? datos.localidad : "";

        //otra forma de hacerlo:
        //datos.hasOwnProperty("nombre") || (datos.nombre="");
        //datos.hasOwnProperty("edad") || (datos.edad=0);
        //datos.hasOwnProperty("localidad") || (datos.localidad="");


        //this.nombre=datos.nombre;
        //this.edad=datos.edad;
        //this.localidad=datos.localidad;

    }
}

const personas = [];
personas.push(new Persona(), new Persona({ nombre: "Ana Gonzalez" }), new Persona({ nombre: "Luis Gomez", edad: 45 }));