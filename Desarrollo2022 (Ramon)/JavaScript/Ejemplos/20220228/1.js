function escribir(argumento) {
    let pantalla = document.querySelector('#display');
    if (argumento == "c") {
        pantalla.innerHTML = "";
    } else {
        pantalla.innerHTML += argumento;
    }
}

function mover(posicion) {
    let selector = document.querySelector('#display');
    let izquierda = window.getComputedStyle(selector).getPropertyValue("left");
    let arriba = window.getComputedStyle(selector).getPropertyValue("top");

    if (posicion == "derecha") {
        selector.style.left = (parseInt(izquierda) + 10).toString() + "px";
    } else if (posicion == "abajo") {

        selector.style.top = (parseInt(arriba) + 10).toString() + "px";

    }
}