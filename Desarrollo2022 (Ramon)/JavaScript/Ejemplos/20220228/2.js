function producto(numero1, numero2) {
    return numero1 * numero2;
}

function suma(numeros) {
    let sumatorio = 0;
    for (let i = 0; i < numeros.length; i++) {
        if (numeros[i] % 2 == 0) {
            sumatorio += numeros[i];
        }
    }
    return sumatorio;
}

multiplicacion = producto(3, 5)
console.log(multiplicacion)

vector = suma([4, 5, 8, 12, 23])
console.log(vector)