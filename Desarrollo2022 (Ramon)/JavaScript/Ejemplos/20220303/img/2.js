let celdas = document.querySelectorAll('td');

//utilizamos el forEach por primera vez (es un metodo, no una instruccion, por eso se escribe de la forma a.forEach)
//a.foreach(function(valor,indice){})

celdas.forEach(function(celda, c) {
    celda.addEventListener("click", function(e) {
        e.target.style.backgroundImage = "url(" + (c + 1) + ".jpg)";
        e.target.innerHTML = "";
    })
});