let celdas = document.querySelectorAll('td');
let imagenes = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg'];

for (let [c, celda] of celdas.entries()) {
    celda.addEventListener("click", function(e) {
        e.target.style.backgroundImage = "url(" + imagenes[c] + ")"; //"url(" + (c + 1) + ".jpg)";
        e.target.innerHTML = "";
    })
}