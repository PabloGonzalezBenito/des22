document.querySelector("#boton").addEventListener("click", (e) => {
    //creo un fragmento de codigo
    const FRAGMENTO = document.createDocumentFragment();

    //creo una etiqueta div
    const ETIQUETA = document.createElement("div");


    for (let i = 1; i <= 3; i++) {
        let foto = document.createElement(`img`);
        foto.src = `img/${ i }.jpg `;
        //añado la foto al fragmento
        FRAGMENTO.appendChild(foto);
    }
    //añado al body el fragmento
    document.querySelector('body').appendChild(FRAGMENTO);
})