window.addEventListener("load", (e) => {
    //este codigo se ejecuta cuando se carga la web
    document.querySelector('button').addEventListener("click", function() {
        //este codigo se ejecuta cuando pulso el boton
        /*document.querySelector("#salida").innerHTML+="<div>Hola Mundo</div>";  esto seria hasta ahora*/

        //creo un div
        let div = document.createElement("div");
        //Creo un nodo de texto
        let texto = document.createTextNode("Hola Mundo");

        div.appendChild(texto);
        document.querySelector('#salida').appendChild(div.cloneNode(true));
        //div.innerHTML = "Hola mundo";
    });
})