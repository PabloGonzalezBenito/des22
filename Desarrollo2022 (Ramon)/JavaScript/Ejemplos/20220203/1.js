//Crear 3 variables
let nombre = "";
let edad = 0;
let altura = 0;

//Pedimos los datos al usuario
nombre = prompt("Introduzca el nombre: ");
edad = parseInt(prompt("Introduzca la edad: ")); //parseInt sirve para que el valor introducido sea tratado como un número, y no como una cadena de caracteres
altura = parseFloat(prompt("Introduzca la altura: "));

//Imprimimos los valores en pantalla, y tambien mediante alerta y consola
alert(nombre);
document.write("La edad tiene un valor de: " + edad);
console.log("La altura es  " + altura);