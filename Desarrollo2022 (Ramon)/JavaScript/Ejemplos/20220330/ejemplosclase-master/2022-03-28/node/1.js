const http = require('http');
const fs= require('fs');
const hostname = 'localhost';
const port = 3000;
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  fs.readFile('./a.html',(err,data)=>{
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(data.toString())
    res.end();
  });
  
});

server.listen(port, hostname, () => {
  console.log(`Server running at   http://${hostname}:${port}/`);
});