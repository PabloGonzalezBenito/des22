let alumnos=[];//creamos un array que mas adelante va a contener a todos los alumnos

document.querySelector('#mas').addEventListener("click",()=>{//escuchar el boton de añadir del HTML y añadir evento
let alumno={//creamos una clase y recogemos los valores del formulario HTML
nombre:document.querySelector('#nombre').value,
nif:document.querySelector('#nif').value,
edad:document.querySelector('#edad').value
};
if(localStorage.getItem("kalumnos"==null)){//la primera vez que se carga la pagina hay que definir kalumnos
    localStorage.setItem("kalumnos",JSON.stringify(alumnos));
}

//añadir alumno al array de alumnos;
alumnos.push(alumno);
localStorage.setItem("kalumnos",JSON.stringify(alumnos))//creamos una clave donde se graba lo que vale la variable alumnos
//para que se quede guardada cuando se actualice la pagina o se cierre.
}); 

document.querySelector('#borra').addEventListener("click",()=>{//ponemos el escuchador al boton de borrar
alumnos=[];
localStorage.removeItem("kalumnos")
});