let persona = {
    nombre: "Ramon",
    altura: 180,
    saludar: function() {
        return "hola";
    }
}

console.log(persona.nombre);

console.log(persona.saludar());

//recorrer con for in
for (let miembro in persona) {
    if (typeof(persona[miembro]) != 'function') {
        console.log(persona[miembro]);
    } else {
        console.log(persona[miembro]());

    }
}