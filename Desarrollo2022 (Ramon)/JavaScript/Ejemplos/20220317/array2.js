let vector = [
    23,
    "ejemplo"
];

//añadir una propiedad al array
vector.color = "rojo";

//utilizando for
for (let c = 0; c < vector.length; c++) {
    console.log(vector[c]);
}

//utilizando foreach
vector.forEach(function(valor) {
    console.log(valor);
})

//utilizando for of
for (let [indice, valor] of vector.entries()) {
    console.log(valor);
}

//utilizando for in
for (let miembro in vector) {
    console.log(vector[miembro]);
}