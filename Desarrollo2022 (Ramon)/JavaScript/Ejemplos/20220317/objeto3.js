/*
la funcion recibe un string 
la funcion me cuenta cuantas veces se repite cada vocal
me retorna un objeto de este tipo
{
    a:0,
    e:0,
    i:0,
    o:0,
    u:0
}
*/

function contar(texto) {
    let vocales = {
        a: 0,
        e: 0,
        i: 0,
        o: 0,
        u: 0
    }

    for (let miembro in texto) {
        if (texto[miembro] == "a" || texto[miembro] == "A") {
            vocales["a"]++;
        } else if (texto[miembro] == "e" || texto[miembro] == "E") {
            vocales["e"]++;
        } else if (texto[miembro] == "i" || texto[miembro] == "I") {
            vocales["i"]++;
        } else if (texto[miembro] == "o" || texto[miembro] == "O") {
            vocales.o++;
        } else if (texto[miembro] == "u" || texto[miembro] == "U") {
            vocales.u++;
        }

    }
    return vocales;
}

console.log(contar("Ejemplo de clase"));