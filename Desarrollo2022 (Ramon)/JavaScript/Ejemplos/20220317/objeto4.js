/*
recibe una serie de numeros
{
    vector1:[2,3,4,5]
    vector2:[2,3,4,6]
    salida:"texto"
}
la funcion me tiene que devolver un objeto de tipo 
{
    suma:[4,6,8,11]
    producto:[4,9,16,30]
    max:6 el numero mas alto de los dos vectores
}
me debe mostrar el valor mas grande en el elemento con id
*/

function operaciones(datos) {
    let resultado = {
        suma: [],
        producto: [],
        max: datos.vector1[0]
    };
    for (let c = 0; c < datos.vector1.length; c++) {

        resultado.suma[c] = datos.vector1[c] + datos.vector2[c];
        resultado.producto[c] = datos.vector1[c] * datos.vector2[c];

        /*if (resultado.max < datos.vector1[c]) {
            resultado.max = datos.vector1[c];
        }
        if (resultado.max < datos.vector2[c]) {
            resultado.max = datos.vector2[c];
        }*/
        resultado.max = Math.max(...datos.vector1, ...datos.vector2);

    }

    //mostrar el maximo en el div
    document.querySelector('#' + datos.salida).innerHTML = resultado.max;


    return resultado;
}
let origen = {
    vector1: [2, 3, 4, 5],
    vector2: [2, 3, 4, 6],
    salida: "salida"
}


console.log(operaciones(origen))
operaciones({
    vector1: [5, 6, 8],
    vector2: [89, 2, 3],
    salida: "barra"
});