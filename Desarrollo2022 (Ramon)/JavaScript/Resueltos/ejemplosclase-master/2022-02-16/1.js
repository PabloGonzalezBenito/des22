// creando un array vacio
let vector = [];
// let vector=new Array();

// introducir con un bucle datos al array
for (let contador = 0; contador < 10; contador++) {
    vector[contador] = contador;
}

document.write(vector);

// recorriendo el array para mostrarlo
for (let contador = 0; contador < vector.length; contador++) {
    document.write(`<div>${vector[contador]}</div>`);
}


/**
 * vamos a crear un array con los li
 */

let elementos = document.querySelectorAll('li');

for (let contador = 0; contador < 5; contador++) {
    // colocamos contenido en todos los li
    elementos[contador].innerHTML += " " + contador;

    // cambiamos el estilo en todos los li
    elementos[contador].style.backgroundColor = "rgba(150, 255, 255, 0.3)";
    elementos[contador].style.border = "1px solid gray";
}

let vectorElementos=[...elementos];

