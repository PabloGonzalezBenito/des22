let vSalida=document.querySelector("#salida");
let anchura=window.getComputedStyle(vSalida).getPropertyValue("width");

// asignado el ancho de la regla en linea a la etiqueta
vSalida.style.width=anchura;


function cambiar(color) {
	let muestra = document.querySelector("#salida");
	muestra.style.backgroundColor = color;
}

function ancho(valor){
	let muestra = document.querySelector("#salida");
	muestra.style.width = (parseInt(muestra.style.width) + valor) + "px";	
}
