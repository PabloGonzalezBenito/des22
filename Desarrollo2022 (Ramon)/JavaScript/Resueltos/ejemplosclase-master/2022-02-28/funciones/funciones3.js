function producto(numero1, numero2) {
    let resultado = 0;
    resultado = numero1 * numero2;
    return resultado;
}

let num1 = 0;
let num2 = 0;
let r = 0;

num1 = 10;
num2 = 20;

r = producto(num1, num2);
// aqui no puedo utilizar las variables creadas dentro de la funcion

document.write("El valor de la variable num1 es " + num1 + "<br>");
document.write(`El valor de la variable num1 es ${num1} <br>`);
document.write("El valor de la variable num2 es " + num2 + "<br>");
document.write(`El valor de la variable num2 es ${num2} <br>`);
document.write(num1 + "*" + num2 + "=" + r);
document.write(`${num1}*${num2}=${r}`);

r = producto(23, 3);
console.log("23*3=" + r);
console.log(`23*3=${r}`)



function suma(numeros) {
    let resultado = 0;
    let c = 0;

    for (c = 0; c < numeros.length; c++) {
        if ((numeros[c] % 2) == 0) {
            //es par
            //resultado=resultado+numeros[c];
            resultado += numeros[c];
        }
    }
    return resultado;
}

/**
 * fuera de la funcion
 */
let valores = [2, 3, 2, 6];

r = suma(valores);

document.write(`El resultado de la suma de los numeros pares es ${r}`);

r = suma([2, 5, 8, 9, 10]);

document.write(`El resultado de la suma de los numeros pares es ${r}`);