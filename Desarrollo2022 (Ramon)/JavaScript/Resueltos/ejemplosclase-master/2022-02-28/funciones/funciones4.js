/**
 * funciones que recibe un array de numeros
 * y devuelve el producto de todos los 
 * numeros pares
 */

function productoPares(numeros) {
    let resultado = 1;

    for (let c = 0; c < numeros.length; c++) {
        if (numeros[c] % 2 == 0) {
            resultado *= numeros[c];
        }
    }

    return resultado;

}


/**
 * funcion que recibe un array de numeros
 * y retorna la suma de todos los numeros
 * impares del array
 * 
 */

function sumaImpares(numeros) {
    let resultado = 0;

    for (let c = 0; c < numeros.length; c++) {
        if (numeros[c] % 2 != 0) {
            resultado += numeros[c];
        }
    }


    return resultado;
}


let posiciones = [1, 5, 9, 8, 7, 3, 2];
let total = 0;

total = productoPares(posiciones); // 8*2
console.log(total);

total = sumaImpares(posiciones); // 1+5+9+7+3
console.log(total);