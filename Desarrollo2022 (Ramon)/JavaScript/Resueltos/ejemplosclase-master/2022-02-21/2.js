function calcular() {
    // en esta variable voy a colocar el nombre escrito
    let dato = null;

    // esta variable tendra el numero de vocales del nombre
    let vocales = 0;

    // leer el nombre escrito
    dato = document.querySelector('#inombre').value;

    dato = dato.toLowerCase(); // coloco el nombre en minusculas

    /**
     * bucle para contar el numero de vocales del nombre
     */
    for (let contador = 0; contador < dato.length; contador++) {
        /*if (
            dato[contador] == 'a' ||
            dato[contador] == 'e' ||
            dato[contador] == 'i' ||
            dato[contador] == 'o' ||
            dato[contador] == 'u'
        ) {
            vocales++;

        }*/

        switch (dato[contador]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                vocales++;
        }


    }

    // escribir el numero de vocales en el input
    document.querySelector('#ivocales').value = vocales;

}