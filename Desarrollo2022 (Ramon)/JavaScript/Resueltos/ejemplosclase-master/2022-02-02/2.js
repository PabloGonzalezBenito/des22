/**
 * definicion de las variables
 */

let radio = 0;
let perimetro = 0;
let area = 0;
let circulo = null; // apuntar al div circulo


/**
 * Introducir los datos
 */

radio = prompt("Introduce el radio", "0");

/**
 * procesamiento de la informacion
 */

perimetro = 2 * Math.PI * radio;
area = Math.PI * Math.pow(radio, 2);


/**
 * Mostrar resultados
 */

document.write("El radio es: ");
document.write(radio);
document.write("<br>");

document.write("El perimetro es:" + perimetro + "<br>");

document.write(`El area es ${area}<br>`);


/**
 * dibujar circulo con radio dado
 */

circulo = document.querySelector("#circulo");
circulo.style.width = radio + "px";
circulo.style.height = radio + "px";
circulo.style.backgroundColor = "red";
circulo.style.borderRadius = radio + "px";