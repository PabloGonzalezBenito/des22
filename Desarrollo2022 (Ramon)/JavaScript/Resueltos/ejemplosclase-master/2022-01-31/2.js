// Creo una constante con mi nombre
const NOMBRE = "Mi nombre";

// creo una variable
let elemento = null;

document.write(NOMBRE);

console.log(NOMBRE);


// sin utilizar variables
// document.querySelector('#caja').innerHTML=NOMBRE;

// creo una variable que apunta al div
elemento = document.querySelector("#caja");

// aprovecho la variable para modificar el div
elemento.innerHTML = NOMBRE;