let promedio = 0; // nota alumno
let descuento = 0; // descuento en moneda
let pago = 0; // pago en moneda

/**
 * crear variables que apuntan a los span
 */

let cInscripcion = null;
let cDescuento = null;
let cPago = null;

/**
 * Constantes a utilizar
 */
const INSCRIPCION = 3000;
const PDESCUENTO = 0.2;

/**
 * asigno las variables a los span
 */

cInscripcion = document.querySelector('#Cinscripcion');
cDescuento = document.querySelector('#cDescuento');
cPago = document.querySelector('#cPago');

/**
 * Pedir la nota del alumno
 */
promedio = prompt("Introduce la nota media");

if (promedio >= 9) {
    descuento = INSCRIPCION * PDESCUENTO;
}

pago = INSCRIPCION - descuento;

/**
 * Mostrar los datos
 */

cDescuento.innerHTML = descuento;
cPago.innerHTML = pago;