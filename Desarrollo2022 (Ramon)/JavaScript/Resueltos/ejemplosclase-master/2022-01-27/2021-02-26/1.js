
var uno=0;
var dos;
let tres;
let cuatro="ejemplo de clase";

let cinco={
	uno: 0,
	dos:10
};

let seis=new String("uno");

class Siete{
	constructor(a,b){
		this.uno=a;
		this.dos=b;
	}
};

let ocho=new Siete(1,2);

let Nueve=function(a,b){
	this.uno=a
	this.dos=b;
}

let diez=new Nueve(2,5);


console.log(typeof(uno));
console.log(typeof(dos));
console.log(typeof(tres));
console.log(typeof(cuatro));
console.log(typeof(cinco));
console.log(cinco.__proto__);
console.log(seis.__proto__);
console.log(typeof(Siete));
console.log(Siete.__proto__);
console.log(typeof(ocho));
console.log(ocho.__proto__);
console.log(ocho.uno);
console.log(typeof(Nueve));
console.log(diez.__proto__);
console.log(diez.uno);
