// VARIABLES
let numero = 0;
let contador = 0;

// constantes
const MAXIMO = 10;
const MINIMO = 0;

// entradas

numero = +prompt("Introduce el numero del que quieres calcular la tabla de multiplicar");

for (contador = MINIMO; contador <= MAXIMO; contador++) {
    document.write(`${numero} x ${contador} = ${numero*contador} <br>`);
}

contador = MINIMO;
while (contador <= MAXIMO) {
    document.write(`${numero} x ${contador} = ${numero*contador} <br>`);
    contador++;
}