// fijando la anchura desde js para poder modificarla 
// desde js
document.querySelector("#salida").style.width = "400px";

function rojo() {
    let muestra = document.querySelector("#salida");
    muestra.style.backgroundColor = "red";
}

function verde() {
    let muestra = document.querySelector("#salida");
    muestra.style.backgroundColor = "green";
}

function azul() {
    let muestra = document.querySelector("#salida");
    muestra.style.backgroundColor = "blue";
}

function incAncho() {
    let muestra = document.querySelector("#salida");
    muestra.style.width = parseInt(muestra.style.width) + 10 + "px";
}

function decAncho() {
    let muestra = document.querySelector("#salida");

    /*muestra.style.width = (parseInt(muestra.style.width) - 10) + "px";*/
    muestra.style.width = (parseInt(muestra.style.width) - 10).toString() + "px";
}