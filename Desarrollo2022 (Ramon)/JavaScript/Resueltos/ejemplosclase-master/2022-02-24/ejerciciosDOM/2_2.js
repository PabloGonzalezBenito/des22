function operar(operacion) {
    let vNumero1 = document.querySelector("#numero1");
    let vNumero2 = document.querySelector("#numero2");
    let vResultado = document.querySelector("#resultado");

    let resultado = 0;

    if (operacion == "sumar") {
        resultado = +vNumero1.value + (+vNumero2.value);
    } else {
        resultado = vNumero1.value - vNumero2.value;
    }
    vResultado.value = resultado;
}