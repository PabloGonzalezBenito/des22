/**
 * Creo dos variables globales que apuntan a las cajas
 */
let vNumero1 = document.querySelector("#numero1");
let vNumero2 = document.querySelector("#numero2");

function sumar() {
    let suma = +vNumero1.value + (+vNumero2.value);
    salida(suma);
}

function restar() {
    let resta = +vNumero1.value - vNumero2.value;
    salida(resta);
}

function salida(valor) {
    document.querySelector("#resultado").value = valor;
}


/*function sumar(){
	document.querySelector('#resultado').value=
	+document.querySelector('#numero1').value+
	(+document.querySelector('#numero2').value);
}

function restar(){
	document.querySelector('#resultado').value=
	document.querySelector('#numero1').value-
	document.querySelector('#numero2').value;
}*/