function sumar() {
    let vNumero1 = document.querySelector("#numero1");
    let vNumero2 = document.querySelector("#numero2");
    let suma = +vNumero1.value + (+vNumero2.value);
    salida(suma);
}

function restar() {
    let vNumero1 = document.querySelector("#numero1");
    let vNumero2 = document.querySelector("#numero2");
    let resta = +vNumero1.value - vNumero2.value;
    salida(resta);
}

function salida(valor) {
    let vResultado = document.querySelector("#resultado");
    vResultado.value = valor;
}