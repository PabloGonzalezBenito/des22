let letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
let DNINumeros = prompt("Introduzca los 8 numeros de su DNI: ");
let letraUsuario = prompt("Introduzca la letra de su DNI: ");
letraUsuario = letraUsuario.toUpperCase()

if (DNINumeros > 99999999 || DNINumeros < 0) { document.write("El número introducido no es válido") } else {
    let resto = DNINumeros % 23;
    DNILetra = letras[resto];
}

if (letraUsuario != DNILetra) { document.write("La letra que ha introducido no es correcta") } else { document.write("El número y la letra del DNI son correctos") }