/*Vamos a crear una clase que recibe 3 argumentos*/

class Caja {

    /*propiedad privada
    var unidad = "px";*/
    constructor(a, b, c) {
            //propiedades publicas
            this.ancho = a;
            this.alto = b;
            this.texto = c;
        }
        /*metodo privado
    function concatenar() {
        this.ancho = this.ancho + unidad;
        this.alto = this.alto + unidad;
    }*/
        //metodos publicos
    mensaje() {
        this.texto = "esto es un ejemplo";
    }
    mostrar() {
        alert(this.texto);
    };
};
const o = new Caja(10, 2, "a")