/*Vamos a crear una clase padre*/
class Persona {
    //propiedad privada
    #nombre="";
    //metodos publicos
    dormir() {
        console.log("ZZZZZZZZZZZZ");
    };
    hablar() {
        console.log("BLA BLA BLA");
    };
    contar() {
        console.log("1 2 3 4 5 6");
    };
get nombre(){
    return this.#nombre;
}
set nombre (valor){
    this.#nombre=valor;
}
}
/*Vamos a crear una clase hija*/
class Hija extends Persona{
    constructor(){
        super();
    this.atributoHijo = 18;
};}
Hija.prototype = new Persona();


/*Vamos a crear un objeto desde la clase hija*/
const ana = new Hija()

/*Vamos a crear un objeto desde la clase padre*/
const ramon = new Persona();
console.log(Ana.atributoHijo);
console.log(ana);
console.log(ramon);