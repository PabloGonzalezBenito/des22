/*Vamos a crear una clase*/
class persona{
    //propiedad privada
    #nombre="";
    //metodos publicos
    
    dormir(){
        console.log("ZZZZZZZZZZZZ");
    };
   hablar(){
        console.log("BLA BLA BLA");
    };
    contar() {
        console.log("1 2 3 4 5 6");
    };
    get nombre() {
        return this.#nombre;
    };
    set nombre(valor) {
        this.#nombre=valor;
    };
;
}
/*Vamos a crear un objeto con una instancia de la clase*/
var alumno = new persona()
alumno.dormir();
alumno.hablar();
alumno.contar();
alumno.nombre="Pepe";
console.log(alumno.nombre);