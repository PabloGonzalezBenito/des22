/*Creamos una clase*/

class Caja {

    //propiedad privada
    #unidad = "px";
    //propiedades publicas
    constructor(ancho, alto, texto) {
            this.ancho = ancho;
            this.alto = alto;
            this.texto = texto;
        }
        //metodo privado
        #concatenar() {
            this.ancho = this.ancho + this.#unidad;
            this.alto = this.alto + this.#unidad;
        }
        //metodos publicos
    mensaje() {
        this.#concatenar();
        this.texto = "esto es un ejemplo";
    }
    mostrar() {
        alert(this.texto);
    };
};

/*Creamos el objeto*/
const objeto = new Caja(10, 20, "hola mundo");
console.log(objeto.alto);
objeto.mostrar();