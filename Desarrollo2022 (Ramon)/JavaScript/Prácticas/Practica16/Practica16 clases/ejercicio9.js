class Animal{
    constructor(){
    this.nombrePadre = "Soy el padre";
};
}
class Leon extends Animal{ //Esta linea nos permite que Leon herede todos los miembros de Animal
    constructor(){
        super(); //con super hacemos que se ejecute el constructor del padre(tiene que ser en la primera linea del constructor del hijo)
    this.nombreHijo = "Soy el hijo";
};
}
const objetoPadre = new Animal();
const objetoHijo = new Leon();

console.log(objetoPadre.nombrePadre);
console.log(objetoHijo.nombrePadre);
console.log(objetoHijo.nombreHijo);