/*Creamos una clase*/

var Caja = function(a, b, c) {

    //propiedad privada
    let unidad = "px";
    //propiedades publicas
    this.ancho = a;
    this.alto = b;
    this.texto = c;

    //metodo privado
    function concatenar() {
        this.ancho = this.ancho + unidad;
        this.alto = this.alto + unidad;
    }
    //metodos publicos
    this.mensaje = function() {
        this.texto = "esto es un ejemplo";
    }
    this.mostrar = function() {
        alert(this.texto);
    };
};

/*Creamos el objeto*/
const objeto = new Caja(10, 20, "hola mundo");
console.log(objeto.alto);
objeto.mostrar();