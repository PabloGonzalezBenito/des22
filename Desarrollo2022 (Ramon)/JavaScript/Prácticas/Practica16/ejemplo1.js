/*Vamos a crear una clase*/
const Persona = function() {
    //propiedad privada
    let nombre;
    //metodos publicos
    this.dormir = function() {
        console.log("ZZZZZZZZZZZZ");
    };
    this.hablar = function() {
        console.log("BLA BLA BLA");
    };
    this.contar = function() {
        console.log("1 2 3 4 5 6");
    };
    this.setNombre = function(valor) {
        nombre = valor;
    };
    this.getNombre = function() {
        return nombre;
    };
};
/*Vamos a crear un objeto con una instancia de la clase*/
const alumno = new persona()
alumno.dormir();
alumno.hablar();
alumno.contar();
alumno.setNombre(prompt("Introduce nombre"));
console.log(alumno.getNombre());