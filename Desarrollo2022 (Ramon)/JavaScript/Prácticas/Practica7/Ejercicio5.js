let numero1 = parseInt(prompt("Introduzca el primer número: "));
let numero2 = parseInt(prompt("Introduzca el segundo número: "));
let numero3 = parseInt(prompt("Introduzca el tercer número: "));
let mayor = Math.max(numero1, numero2, numero3);
let menor = Math.min(numero1, numero2, numero3);

document.querySelector("#variable1").innerHTML = numero1;
document.querySelector("#variable2").innerHTML = numero2;
document.querySelector("#variable3").innerHTML = numero3;
document.querySelector("#mayor").innerHTML = mayor;
document.querySelector("#menor").innerHTML = menor;