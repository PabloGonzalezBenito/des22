class Animal {
    constructor(){
    //propiedad no estatica
    this.peso = 0;
}
    //propiedad estatica
    static pelo = "si";
    //metodo no estatico
    comer() {
        console.log("ÑAN");
    };
    //metodo estatico
    static estatico() {
        this.peso=100;
        console.log("esto es un elemento de la clase");
    };
    static crear(){
        new this;
    }
};

/*Produce error ya que es un metodo de la clase y no del objeto leon.estatico();*/
Animal.estatico();
console.log(Animal.pelo);
/*Esto esta vacio porque es un elemento de la clase y no del objeto
console.log(leon.pelo);*/