class Div{
//privado (un miembro privado siempre se lee con el getter, no hay otra opcion)
    #texto="hola";
            //propiedades publicas

    constructor(){
        this._borde=1;  //el guion bajo es para poder acceder al elemento a traves del set; 
        //en caso contrario el programa entra en un bucle infinito
        this.fondo="RED";
    }

    get borde(){
    return this._borde
}
set borde(valor){
    this._borde=valor;
}

get texto(){
    return this.#texto  //this porque es un miembro de la clase (miembros son metodos y propiedades)
}

set texto(valor){
    this.#texto=valor;
}
get fondo(){
    return `backgroundColor=${this._fondo}`
}
set fondo(valor){
    this._fondo=valor;
}
}

const caja1=new Div();

console.log(caja1.borde);