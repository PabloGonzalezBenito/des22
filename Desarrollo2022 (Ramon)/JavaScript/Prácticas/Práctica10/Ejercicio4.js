/*Escribir un programa que pida 10 números enteros por teclado y que imprima por pantalla:
 Cuántos de esos números son pares.
 Cuál es el valor del número máximo.
 Cuál es el valor del número mínimo.*/


let vector = [];
let solucion = []
let numero = []
let coincide1 = 0;
let coincide2 = 0;
for (i = 0; i < 10; i++) {
    vector[i] = parseInt(prompt("Introduzca un numero: "));
}

for (i = 0; i < 2; i++) {
    numero[i] = parseInt(prompt("Introduzca un numero del que se comprobará si está entre los anteriores: "));
}

for (i = 0; i < 10; i++) {
    if (numero[0] == vector[i]) {
        coincide1 = 1;
    } else if (numero[1] == vector[i]) {
        coincide2 = 1;
    }
}

if (coincide1 == 1) {
    document.write("El primer número a comprobar coincide con alguno de los 10 números introducidos anteriormente." + "<br>");
}
if (coincide2 == 1) {
    document.write("El segundo número a comprobar coincide con alguno de los 10 números introducidos anteriormente.");
}