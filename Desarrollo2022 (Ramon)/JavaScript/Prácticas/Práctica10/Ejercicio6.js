/*function countVowel(str) {

    // find the count of vowels
    const count = str.match(/[aeiou]/gi).length;

    // return number of vowels
    return count;
}

// take input
const string = prompt('Enter a string: ');

const result = countVowel(string);

ratio = result / string.length;
document.write("La relación del número de vocales con respecto al total de caracteres es: " + ratio);*/

let texto = prompt("Introduce un texto: ");
let vocales = [0, 0, 0, 0, 0];
let vector = texto.split("");
//Convertir el texto en un array
for (contador = 0; contador < texto.length; contador++) {
    if (texto[contador] == "a" || texto[contador] == "A" || texto[contador] == "á" || texto[contador] == "Á") {
        vocales[0]++;

    } else if (texto[contador].toLowerCase() == "e" || texto[contador].toLowerCase() == "é") {
        vocales[1]++;
    } else if (texto[contador].toLowerCase() == "i" || texto[contador].toLowerCase() == "í") {
        vocales[2]++;
    } else if (texto[contador].toLowerCase() == "o" || texto[contador].toLowerCase() == "ó") {
        vocales[3]++;
    } else if (texto[contador].toLowerCase() == "u" || texto[contador].toLowerCase() == "ú") {
        vocales[4]++;
    }
}

for (contador = 0; contador < 5; contador++) {
    document.write(vocales[contador] + "<br>");
}