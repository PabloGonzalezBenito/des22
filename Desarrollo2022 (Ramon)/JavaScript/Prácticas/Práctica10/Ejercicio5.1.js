/*Escribir un programa que pida 10 números enteros por teclado y que imprima por pantalla:
 Cuántos de esos números son pares.
 Cuál es el valor del número máximo.
 Cuál es el valor del número mínimo.*/


let positivos = 0;
let negativos = 0;
let numeros = [-2, -3, 0, 2, 4, 6]

positivos = numeros.filter(pos);

function pos(numero) {
    return numero >= 0;
}

negativos = numeros.filter(neg);

function neg(numero) {
    return numero < 0;
}

function operar(numeros) {
    for (numero of numeros)
        resultado += numero;
}


suma = positivos.foreach(operar);

resta = negativos.foreach(operar);


document.write("La suma de números positivos es: " + suma + "<br>");
document.write("La suma de números negativos es: " + resta);